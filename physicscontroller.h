#ifndef PHYSICSCONTROLLER_H
#define PHYSICSCONTROLLER_H

#include <memory>
#include <scene/event/gmeventcontroller.h>
#include "myphyslib/physLib/physentity.h"
#include "myphyslib/physLib/collision.h"
#include "collisionevent.h"

class PhysicsController : public GMlib::EventController
{
public:
    PhysicsController();
    ~PhysicsController();

    void addEntity(PhysEntity* e);

    //double getFirstEventX(){}

    //bool detectEvents(double dt);
    //void handleFirstEvent();
    //void finalize();

private:
    std::vector< std::shared_ptr<PhysEntity> > _dynamic_objects;
    std::vector< std::shared_ptr<PhysEntity> > _static_objects;
    std::vector< CollisionEvent > _collisions;

    float _timespan;
    float _x;

    //Inherited
    void clear() override;
    bool detect(double dt) override;
    void handleFirst() override;
    void doFinalize() override;
    double getFirstX() const;
};

#endif
