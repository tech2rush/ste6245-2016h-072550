#include "scenario.h"

#include "testtorus.h"


//My includes

#include "physicscontroller.h"

#include "myphyslib/physLib/plane.h"
#include "myphyslib/physLib/ball.h"

//// hidmanager
//#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>

// qt
#include <QQuickItem>



void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::White, GMlib::GMcolor::White,
                                                     GMlib::GMcolor::White, init_light_pos );
  light->setAttenuation(0.8, 0.002, 0.0008);
  scene()->insertLight( light, false );

  // Insert Sun
  scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3> init_cam_pos(  0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  0.0f, 0.0f, 1.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Front cam
  auto front_rcpair = createRCPair("Front");
  front_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, -50.0f, 0.0f ), init_cam_dir, init_cam_up );
  front_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( front_rcpair.camera.get() );
  front_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Side cam
  auto side_rcpair = createRCPair("Side");
  side_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( -50.0f, 0.0f, 0.0f ), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ), init_cam_up );
  side_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( side_rcpair.camera.get() );
  side_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Top cam
  auto top_rcpair = createRCPair("Top");
  top_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, 0.0f, 50.0f ), -init_cam_up, init_cam_dir );
  top_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( top_rcpair.camera.get() );
  top_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );







  // Surface visualizers
//  auto surface_visualizer = new GMlib::PSurfNormalsVisualizer<float,3>;


  // Surface
  /*
  auto surface = new TestTorus;
  surface->toggleDefaultVisualizer();
  surface->insertVisualizer(surface_visualizer);
  surface->replot(200,200,1,1);
  scene()->insert(surface);

  surface->test01();
  */


  GMlib::EventManager* eManager = new GMlib::EventManager();
  scene()->setEventManager(eManager);

  auto pController = new PhysicsController();
  eManager->registerController(pController);

  //Insert world bounding box
  //Insert visual edges. Extended from plane.h?
  //Add ball and test
    float w = 10.0f;
    float h = w;

//Ground
    auto plane = new GMlib::PPlane<float>(
                GMlib::Point<float,3>{-w/2,-h/2,0},
                GMlib::Vector<float,3>{w,0,0},
                GMlib::Vector<float,3>{0,h,0});
    plane->setColor(GMlib::GMcolor::Azure);
    plane->toggleDefaultVisualizer();
    plane->replot(10,10,1,1);
    scene()->insert(plane);

    std::cout << plane->getCenterPos() << " * " << plane->getNormal() << std::endl;

    //Physical ground
    auto ground = new Plane(plane->getCenterPos(), plane->getNormal());
    //ground->setStaticObject(true);
    pController->addEntity(ground);

    //Board edges
    std::vector< GMlib::PPlane<float>* > edges;
    edges.emplace_back(new
                GMlib::PPlane<float>(
                GMlib::Point<float,3>{w/2, -h/2,0},
                GMlib::Vector<float,3>{0, 0, h},
                GMlib::Vector<float,3>{0, h, 0}));//Right side

    edges.emplace_back(new
                GMlib::PPlane<float>(
                GMlib::Point<float,3>{-w/2, -h/2, 0},
                GMlib::Vector<float,3>{0, h, 0},
                GMlib::Vector<float,3>{0, 0, h}));//Left side
    edges.emplace_back(new
                GMlib::PPlane<float>(
                GMlib::Point<float,3>{-w/2, h/2, 0},
                GMlib::Vector<float,3>{w, 0, 0},
                GMlib::Vector<float,3>{0, 0, h}));//Top
    edges.emplace_back(new
                GMlib::PPlane<float>(
                GMlib::Point<float,3>{w/2, -h/2, 0},
                GMlib::Vector<float,3>{-w, 0, 0},
                GMlib::Vector<float,3>{0, 0, h}));//Top


    for (GMlib::PPlane<float>* edge: edges)
    {
        edge->toggleDefaultVisualizer();
        edge->replot(10,10,1,1);
        scene()->insert(edge);
    }
    //Manually adding for reasons
    pController->addEntity(new Plane(GMlib::Point<float,3>{w/2, -h/2, 0.0}, edges.at(0)->getNormal()));
    pController->addEntity(new Plane(GMlib::Point<float,3>{-w/2, -h/2, 0.0}, edges.at(1)->getNormal()));
    pController->addEntity(new Plane(GMlib::Point<float,3>{-w/2, h/2, 0.0}, edges.at(2)->getNormal()));
    pController->addEntity(new Plane(GMlib::Point<float,3>{w/2, -h/2, 0.0}, edges.at(3)->getNormal()));

}

void Scenario::cleanupScenario() {

}
